#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
  char *data;
  struct Node *above;
  struct Node *below;
} Node;

Node *top_of_stack = NULL;

Node *item(char *data);
void top();
void *push(char *data);
void *pop();
int print_stack();

Node *item(char *data)
{
  Node *node = (Node *)malloc(sizeof(Node));
  node->data = data;
  node->below = node->above = NULL;
  return node;
}

void top()
{
  printf("Top of Stack is %s\n", top_of_stack->data);
}

void *push(char *data)
{
  Node *new_node = item(data);
  if (top_of_stack == NULL)
  {
    top_of_stack = new_node;
    return NULL;
  }
  new_node->below = top_of_stack;
  top_of_stack->above = new_node;
  top_of_stack = new_node;
}

void *pop()
{
  if (top_of_stack->below == NULL)
  {
    printf("Stack Underflow\n");
    exit(1);
  }
  top_of_stack = top_of_stack->below;
  free(top_of_stack->above);
}

int print_stack()
{
  int i = 0;
  Node *current = top_of_stack;
  printf("------STACK-----\n");
  while (current != NULL)
  {
    printf("|     %s    |\n", current->data);
    printf("____________\n");
    current = current->below;
    i++;
  }
  return i;
}
