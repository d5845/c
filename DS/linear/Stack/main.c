#include <stdio.h>
#include <stdlib.h>
#include "stack.c"

void push_operation();
void pop_operation();
void length();

void push_operation()
{
	int n = 0;
	char *value;
	printf("Enter number of elements to push : \n");
	scanf("%d", &n);
	push("AAAAAAAAAAAA");
	push("AAAAAAAAAAAAAAAAAA");
	// while (n > 0)
	// {
	// 	printf("Enter value : ");
	// 	scanf("%[^\n]s", value);
	// 	push(value);
	// 	n--;
	// }
}

void pop_operation()
{
	int n = 0;
	printf("Enter number of elements to pop : \n");
	scanf("%d", &n);
	while (n-- > 0)
		pop();
}

void length()
{
	int len = print_stack();
	printf("Size of Stack is %d \n", len);
}

int main()
{
	short int input = 1;
	while (input != 0)
	{
		printf("SELECT OPTIONS BELOW TO PERFORM AN OPERATION:\n");
		printf("1)PRINT STACK\n");
		printf("2)PUSH\n");
		printf("3)POP\n");
		printf("4)TOP\n");
		printf("5)LENGTH\n");
		printf("0)EXIT\n");
		scanf("%hd", &input);

		switch (input)
		{
		case 0:
			return 0;
			break;
		case 1:
			print_stack();
			break;
		case 2:
			push_operation();
			break;
		case 3:
			pop_operation();
			break;
		case 4:
			top();
			break;
		case 5:
			length();
			break;
		default:
			printf("Invalid selection, try again\n");
			continue;
		}
	}
	return 0;
}
