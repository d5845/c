#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>

struct Stack{
	int tos;
  int array[5];
};

int top(struct Stack stack){
	return stack.array[stack.tos];
}

void push(struct Stack stack, int value){
	stack.array[stack.tos] = value;
	stack.tos++;
	printf("Pushed %i to stack\n",stack.array[stack.tos]);
}

int pop(struct Stack stack){
	printf("Removed %i from stack", stack.array[stack.tos]);
	stack.tos--;
}


void perform_push_operations(struct Stack stack){
	int num_items, value;
	printf("Enter number of elements to push :");
	scanf("%d",&num_items);

	for(int i = 0; i < num_items; i++){
		if(stack.tos < (sizeof(stack.array)/sizeof(stack.array[0]))){
			printf("Enter value %i :", i);
			scanf("%i", &value);
			push(stack, value);
		}
		else{
			printf("Stack Overflow");
		}
	}

	
}

void perform_pop_operations(struct Stack stack){
	int num_items;
	printf("Enter number of items to pop :");
	scanf("%d",&num_items);

	for(int i =0; i < num_items; i++){
		pop(stack);
	}
}

int main(void){
	struct Stack stack;
	short int input=1;
	while(input!=0){
		printf("SELECT OPTIONS BELOW TO PERFORM AN OPERATION:\n");
		printf("1)PUSH\n");
		printf("2)POP\n");
		printf("3)TOP\n");
		printf("0)EXIT\n");
		scanf("%hd",&input);
		switch(input){
			case 0:return 0;
			break;
			case 1:perform_push_operations(stack);
			break;
			case 2:perform_pop_operations(stack);
			break;
			case 3:printf("Top most item on stack is: %d",top(stack));
			break;
			default:printf("Invalid Selection");
		}
	}


	return 0;
}
