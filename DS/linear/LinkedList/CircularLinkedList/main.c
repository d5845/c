#include <stdio.h>
#include <stdlib.h>
#include "circularlinkedlist.c"

void display_list();
void append_operation();
void insert_operation();
void remove_operation();
void length();

void display_list()
{
	printf("------CIRCULAR LINKED LIST-----\n");
	traverse();
}

void append_operation()
{
	int n = 0;
	int value = 0;
	printf("Enter number of elements to append : \n");
	scanf("%d", &n);
	while (n > 0)
	{
		printf("Enter value : ");
		scanf("%d", &value);
		append(value);
		n--;
	}
}

void insert_operation()
{
	int n = 0;
	int value = 0;
	int index = 0;
	printf("Enter number of elements to insert : \n");
	scanf("%d", &n);
	while (n > 0)
	{
		printf("Enter value : ");
		scanf("%d", &value);
		printf("Enter index : ");
		scanf("%d", &index);
		insert(value, index);
		n--;
	}
}

void remove_operation()
{
	int n = 0;
	int index = 0;
	printf("Enter number of elements to remove : \n");
	scanf("%d", &n);
	while (n > 0)
	{
		printf("Enter index  : ");
		scanf("%d", &index);
		remove_node(index);
		n--;
	}
}

void length()
{
	int len = traverse();
	printf("Length of list is %d \n", len);
}

int main()
{
	short int input = 1;
	while (input != 0)
	{
		printf("SELECT OPTIONS BELOW TO PERFORM AN OPERATION:\n");
		printf("1)DISPLAY\n");
		printf("2)APPEND\n");
		printf("3)INSERT\n");
		printf("4)REMOVE\n");
		printf("5)LENGTH\n");
		printf("0)EXIT\n");
		scanf("%hd", &input);

		switch (input)
		{
		case 0:
			return 0;
			break;
		case 1:
			display_list();
			break;
		case 2:
			append_operation();
			break;
		case 3:
			insert_operation();
			break;
		case 4:
			remove_operation();
			break;
		case 5:
			length();
			break;
		default:
			printf("Invalid selection, try again\n");
			continue;
		}
	}
	return 0;
}
