#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
  int data;
  struct Node *next;
} Node;

Node *head = NULL;

Node *node(int data);
int traverse(int print);
void *append(int data);
void *insert(int data, int index);
Node *remove_node(int index);

Node *node(int data)
{
  Node *node = (Node *)malloc(sizeof(Node));
  node->data = data;
  node->next = NULL;
  return node;
}

int traverse(int print)
{
  int len = 0;
  Node *current_node = head;
  while (current_node != NULL)
  {
    if(print == 1)
      printf("%d -> ", current_node->data);
    current_node = current_node->next;
    len++;
  }
  printf("\n");
  return len;
}

void *append(int data)
{
  if (head == NULL)
  {
    head = node(data);
    return NULL;
  }

  Node *current_node = head;
  while (current_node->next != NULL)
    current_node = current_node->next;
  current_node->next = node(data);
}

void *insert(int data, int index)
{
  int i = 0;
  Node *current_node = head;
  while (current_node->next != NULL)
  {
    if (i == index - 1)
    {
      Node *next_node = current_node->next;
      Node *new_node = node(data);
      current_node->next = new_node;
      new_node->next = next_node;
    }
    i++;
    current_node = current_node->next;
  }
}

Node *remove_node(int index)
{
  Node *current_node = head;
  int i = 0;

  while (current_node->next != NULL)
  {
    if (i == index - 1)
    {

      Node *previous_node = current_node;
      previous_node->next = current_node->next;
      current_node = NULL;
      return NULL;
    }
    current_node = current_node->next;
    i++;
  }
  return head;
}