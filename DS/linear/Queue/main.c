#include <stdio.h>
#include <stdlib.h>
#include "queue.c"

#define TRUE 1
#define FALSE 0

void display_list();
void enqueue_operation();
void dequeue_operation();
void print_front();
void print_rear();
void length();

void display_queue()
{
	printf("------QUEUE-----\n");
	traverse(1);
	printf("^\n");
	printf("| front\n\n");
}

void enqueue_operation()
{
	int n = 0;
	int value = 0;
	printf("Enter number of elements to enqueue : \n");
	scanf("%d", &n);
	while (n > 0)
	{
		printf("Enter value : ");
		scanf("%d", &value);
		enqueue(value);
		n--;
	}
}

void dequeue_operation()
{
	int n = 0;
	printf("Enter number of elements to dequeue : \n");
	scanf("%d", &n);
	while (n > 0)
	{
		dequeue();
		n--;
	}
}

void print_front()
{
	int front_node_data = get_front()->data;
	printf("Front of Queue is %d \n", front_node_data);
}

void print_rear()
{
	int rear_node_data = get_rear()->data;
	printf("Rear of Queue is %d \n", rear_node_data);
}

void length()
{
	int len = traverse(0);
	printf("Size of Queue is %d \n", len);
}

int main()
{
	short int input = 1;
	while (input != 0)
	{
		printf("SELECT OPTIONS BELOW TO PERFORM AN OPERATION:\n");
		printf("1)PRINT QUEUE\n");
		printf("2)ENQUEUE\n");
		printf("3)DEQUEUE\n");
		printf("4)FRONT\n");
		printf("5)REAR\n");
		printf("6)LENGTH\n");
		printf("0)EXIT\n");
		scanf("%hd", &input);

		switch (input)
		{
		case 0:
			return 0;
			break;
		case 1:
			display_queue();
			break;
		case 2:
			enqueue_operation();
			break;
		case 3:
			dequeue_operation();
			break;
		case 4:
			print_front();
			break;
		case 5:
			print_rear();
			break;
		case 6:
			length();
			break;
		default:
			printf("Invalid selection, try again\n");
			continue;
		}
	}
	return 0;
}
